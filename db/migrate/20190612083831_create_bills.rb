class CreateBills < ActiveRecord::Migration[5.1]
  def change
    create_table :bills do |t|
      t.string :brand_manager
      t.string :narration
      t.date :invoice_date
      t.float :amount
      t.string :customer_name
      t.string :reference
      t.timestamps
    end
  end
end
