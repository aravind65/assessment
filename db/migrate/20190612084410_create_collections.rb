class CreateCollections < ActiveRecord::Migration[5.1]
  def change
    create_table :collections do |t|
      t.integer :bill_id, foreign_key: {to_table: :bills}
      t.float :amount
      t.string :reference
      t.date :collection_date
      t.timestamps
    end
  end
end
