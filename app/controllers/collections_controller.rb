class CollectionsController < ApplicationController
  def index
    @collections = Collection.all.group_by(&:bill_id)
  end

  def create

  end

  def collection_path
    if params[:file].present?
      @data.each do |data|
        @invoice = Bill.find_by(reference: data['referbundle installence'])
        if @invoice.present?
          Collection.find_or_create_by(bill_id: @invoice.id,
                                       amount: data['amount'],
                                       reference: data['reference'],
                                       collection_date: data['collection_date'],
          )
        else
          Collection.find_or_create_by(bill_id: nil,
                                       amount: data['amount'],
                                       reference: data['reference'],
                                       collection_date: data['collection_date'],
          )
        end
      end
    end
  end

  private

  def set_collection
    @collection = Collection.find_by(id: params[:id])
  end

  def collection_params
    params.require(:collection).permit(:collection_date, :amount, :reference)
  end
end
