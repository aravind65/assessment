class InvoicesController < ApplicationController

  before_action :set_invoice, only: [:show, :edit]

  def index
    @invoices = Bill.all
  end

  def edit

  end

  def show

  end

  def create
    @invoice = Bill.new(invoice_params)
    if @invoice.save
      flash[:success] = "successful"
    else
      flash[:error] = @invoice.errors.full_messages.to_sentence
    end
  end

  def update
    @invoice.update(invoice_params)
  end

  def upload_file
    if params[:file].present?
      @data = JSON.parse(File.open(params[:file].tempfile).read)
      @data.each do |data|
        Bill.find_or_create_by(
            brand_manager: data['brand_manager'],
            narration: data['narration'],
            invoice_date: data['invoice_date'],
            amount: data['amount'],
            customer_name: data['customer_name'],
            reference: data['reference']
        )
      end

    end
  end

  private

  def set_invoice
    @invoice = Bill.find_by(id: params[:id])
  end

  def invoice_params
    params.require(:invoice).permit(:brand, :narration, :invoice_date, :amount, :customer_name, :reference)
  end

end
