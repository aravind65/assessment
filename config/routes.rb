Rails.application.routes.draw do

  root 'invoices#index'
  resources :invoices, only: [:index, :show, :edit, :update, :create]
  post 'invoice/upload_file', to: 'invoices#upload_file', as: 'upload_invoice'
  resources :collections, only: [:index, :show, :edit, :update, :create]
  post 'collection/collection_file', to: 'collections#collection_file', as: 'upload_collection'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
