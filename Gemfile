source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.1.6'
gem 'puma'
gem 'redis'
gem 'sidekiq'
gem 'sinatra', require: false, github: 'sinatra'
gem 'bcrypt', '~> 3.1.7'
gem 'devise'
gem 'turbolinks'
gem 'sass-rails'
gem 'uglifier', '>= 1.3.0'
gem 'sprockets'
gem 'local_time'
gem 'coffee-rails', '~> 4.2'
gem 'activerecord-import', require: false
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'axlsx', '~> 2.0'
gem 'axlsx_rails'
gem 'roo'
gem 'fuzzy_match'
gem 'amatch'
gem 'vuejs-rails'
gem 'chosen-rails'
gem 'underscore-rails'
gem 'intercom-rails'
gem 'active_model_serializers'
gem 'rack-cors'
gem 'newrelic_rpm'
gem 'rack-mini-profiler'
gem 'faker'
gem 'wicked_pdf'
gem 'wkhtmltopdf-heroku'
gem 'wkhtmltopdf-binary-edge', '~> 0.12.5.0'
gem 'bootsnap'
gem 'typhoeus', require: false
gem 'sax-machine', require: false
gem 'geocoder'
gem 'bulma-rails', '~> 0.7.5'

gem 'aws-sdk-s3', require: false
gem 'mini_magick'
gem "recaptcha", require: "recaptcha/rails"
gem 'premailer-rails'
gem 'combine_pdf'
gem 'nprogress-rails'
gem 'font-awesome-sass'
gem 'histogram'
gem 'parsley-rails'
gem 'griddler'
gem 'griddler-sendgrid'
gem 'google-cloud-vision', "~> 0.31.0", require: false
gem 'pg_search'
gem 'lex'
gem 'redis-namespace'
gem 'hiredis'
gem 'fugit', '~> 1.2.1'
gem 'cronex'
gem 'sidekiq-cron', '~> 1.1.0'
gem 'clowne'
gem 'fast_excel'
gem 'pg'

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'pry'
  gem 'pry-rails'
  gem 'timecop'
  gem 'terminal-table', require: false
end

group :test do
  gem 'capybara'
  gem 'launchy'
  gem 'poltergeist'
  gem 'selenium-webdriver', '~> 2.53.4'
  gem 'chromedriver-helper'
end

group :development do
  gem 'web-console'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end


gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

ruby '2.5.1'
